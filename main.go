package main

import ("log"
	"time"
	"net/http")

func main() {
	
	StartServer()
}


func StartServer() {
	
	s := &http.Server{
		Addr:           "172.22.51.34:8080",
		Handler:        Router{},
		ReadTimeout:    1 * time.Second,
		WriteTimeout:   1 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	
	log.Fatal(s.ListenAndServe())
}











